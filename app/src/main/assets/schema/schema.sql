/*
 Navicat Premium Data Transfer

 Source Server         : PostBar
 Source Server Type    : SQLite
 Source Server Version : 3017000
 Source Schema         : main

 Target Server Type    : SQLite
 Target Server Version : 3017000
 File Encoding         : 65001

 Date: 07/06/2020 17:11:04
*/

PRAGMA foreign_keys = false;

-- ----------------------------
-- Table structure for android_metadata
-- ----------------------------
DROP TABLE IF EXISTS "android_metadata";
CREATE TABLE "android_metadata" (
  "locale" TEXT
);

-- ----------------------------
-- Records of "android_metadata"
-- ----------------------------
INSERT INTO "android_metadata" VALUES ('zh_CN_#Hans');

-- ----------------------------
-- Table structure for circle
-- ----------------------------
DROP TABLE IF EXISTS "circle";
CREATE TABLE "circle" (
  "id" integer PRIMARY KEY AUTOINCREMENT,
  "createtime" integer,
  "deletetime" integer,
  "likenumber" integer,
  "previewimageurl" text,
  "synopsis" text,
  "title" text,
  "updatetime" integer,
  "userid" integer
);

-- ----------------------------
-- Records of "circle"
-- ----------------------------
INSERT INTO "circle" VALUES (1, 1591515881570, NULL, 0, 'http://yun918.cn/study/public/uploadfiles/img/1591515828434.jpg', '数学是研究数量、结构、变化、空间以及信息等概念的一门学科，从某种角度看属于形式科学的一种。', '数学', NULL, 1);
INSERT INTO "circle" VALUES (2, 1591518175118, NULL, 0, 'http://yun918.cn/study/public/uploadfiles/img/1591518054574.jpg', '音乐是一种艺术形式和文化活动，其媒介是按时组织的声音。', '音乐', NULL, 1);
INSERT INTO "circle" VALUES (3, 1591518697542, NULL, 0, 'http://yun918.cn/study/public/uploadfiles/img/1591518656164.jpg', '电影，是由活动照相术和幻灯放映术结合发展起来的一种连续的影像画面，是一门视觉和听觉的现代艺术。', '电影', NULL, 2);
INSERT INTO "circle" VALUES (4, 1591519128283, NULL, 0, 'http://yun918.cn/study/public/uploadfiles/img/1591519018998.jpg', 'Java是一门面向对象编程语言,具有简单性、分布式、健壮性、安全性、平台独立与可移植性等特点。', 'Java', NULL, 2);
INSERT INTO "circle" VALUES (5, 1591519284320, NULL, 0, 'http://yun918.cn/study/public/uploadfiles/img/1591519022251.jpg', '安卓是一种基于Linux内核的开源操作系统，由Google公司和开放手机联盟领导及开发。', 'Android', NULL, 2);
INSERT INTO "circle" VALUES (6, 1591519521788, NULL, 0, 'http://yun918.cn/study/public/uploadfiles/img/1591519467208.jpg', '游戏有智力游戏和活动性游戏之分，游戏多指各种平台上的电子游戏。', '游戏', NULL, 2);

-- ----------------------------
-- Table structure for comment
-- ----------------------------
DROP TABLE IF EXISTS "comment";
CREATE TABLE "comment" (
  "id" integer PRIMARY KEY AUTOINCREMENT,
  "circleid" integer,
  "commenttime" integer,
  "content" text,
  "likenumber" integer,
  "storeyid" integer,
  "targettype" text,
  "targetuserid" integer,
  "userid" integer
);

-- ----------------------------
-- Table structure for dynamic
-- ----------------------------
DROP TABLE IF EXISTS "dynamic";
CREATE TABLE "dynamic" (
  "id" integer PRIMARY KEY AUTOINCREMENT,
  "circle" text,
  "commentnumber" integer,
  "content" text,
  "createtime" integer,
  "deletetime" integer,
  "likenumber" integer,
  "previewimageurl" text,
  "starnumber" integer,
  "title" text,
  "updatetime" integer,
  "userid" integer
);

-- ----------------------------
-- Records of "dynamic"
-- ----------------------------
INSERT INTO "dynamic" VALUES (1, '学习', 0, '+ 列表一
+ 列表二
    + 列表二-1
    + 列表二-2
    + 列表二-3
+ 列表三
    * 列表一
    * 列表二
    * 列表三', 1591406609777, NULL, 1, '', 0, '展示多级标题', NULL, 1);
INSERT INTO "dynamic" VALUES (2, '数学', 0, '$$\(\sqrt{3x-1}+(1+x)^2\)$$ 
$$\sin(\alpha)^{\theta}=\sum_{i=0}^{n}(x^i + \cos(f))$$ 
$$(\sum\_{k=1}^n a\_k b\_k)^2\leq(\sum\_{k=1}^n a\_k^2)(\sum\_{k=1}^n b\_k^2)$$ ', 1591406791737, NULL, 1, '', 0, '发布数学公式', 1591412269796, 2);
INSERT INTO "dynamic" VALUES (3, '游戏', 0, '###论法师二技能的正确用法
![](http://www.westudy.ltd/upload/1/png/d5d71311-2e11-4f75-870b-57bb4983bf2b.png)
## 
![](http://www.westudy.ltd/upload/1/png/92caf9ac-45b8-4655-8f61-5d7632d0124b.png)
####一、法师二技能图片
![](http://www.westudy.ltd/upload/1/jpg/4ccaae9b-d140-4b42-aedc-d7bb29c89ed0.jpg)
#####可以看出法师在有散弹天赋加成的情况下，二技能释放5道冰刺，给予若干秒的冰冻效果，正常情况下是三道冰刺  
![](http://www.westudy.ltd/upload/1/png/92caf9ac-45b8-4655-8f61-5d7632d0124b.png)  
####二、法师二技能搭配天赋
<b>①散弹数量增加————</b>使得原来的三道冰刺变成五道  
![](http://www.westudy.ltd/upload/1/png/b208b2a4-6328-41b5-b83f-774bb6f30e2a.png)  
<b>②冷却缩减————</b>使得二技能CD变短  
![](http://www.westudy.ltd/upload/1/jpg/1f7c58f5-a085-4296-9b62-500c57b491e9.jpg)  
<b>③冰盾————</b>使得使得冰冻时间增加  
![](http://www.westudy.ltd/upload/1/jpg/1f7c58f5-a085-4296-9b62-500c57b491e9.jpg)  
## 
![](http://www.westudy.ltd/upload/1/png/92caf9ac-45b8-4655-8f61-5d7632d0124b.png)  
###如果觉得不错的话，留下你的赞哦！', 1591412909414, NULL, 0, 'http://www.westudy.ltd/upload/1/png/d5d71311-2e11-4f75-870b-57bb4983bf2b.png', 0, '图文展示', NULL, 3);
INSERT INTO "dynamic" VALUES (4, '学习', 0, '#修改成功', 1591415257849, NULL, 0, '', 0, '测试修改', 1591440593084, 7);

-- ----------------------------
-- Table structure for hide
-- ----------------------------
DROP TABLE IF EXISTS "hide";
CREATE TABLE "hide" (
  "id" integer PRIMARY KEY AUTOINCREMENT,
  "state" integer,
  "targetid" integer,
  "userid" integer,
  "targettype" integer
);

-- ----------------------------
-- Records of "hide"
-- ----------------------------
INSERT INTO "hide" VALUES (1, 1, 4, 2, 3);

-- ----------------------------
-- Table structure for like
-- ----------------------------
DROP TABLE IF EXISTS "like";
CREATE TABLE "like" (
  "id" integer PRIMARY KEY AUTOINCREMENT,
  "isliked" integer,
  "targetid" integer,
  "targettype" text,
  "userid" integer
);

-- ----------------------------
-- Records of "like"
-- ----------------------------
INSERT INTO "like" VALUES (1, 1, 1, 'star dynamic', 2);
INSERT INTO "like" VALUES (2, 1, 2, 'star dynamic', 2);
INSERT INTO "like" VALUES (3, 0, 3, 'star dynamic', 2);

-- ----------------------------
-- Table structure for sqlite_sequence
-- ----------------------------
DROP TABLE IF EXISTS "sqlite_sequence";
CREATE TABLE "sqlite_sequence" (
  "name",
  "seq"
);

-- ----------------------------
-- Records of "sqlite_sequence"
-- ----------------------------
INSERT INTO "sqlite_sequence" VALUES ('table_schema', 8);
INSERT INTO "sqlite_sequence" VALUES ('user', 8);
INSERT INTO "sqlite_sequence" VALUES ('dynamic', 4);
INSERT INTO "sqlite_sequence" VALUES ('hide', 1);
INSERT INTO "sqlite_sequence" VALUES ('star', 2);
INSERT INTO "sqlite_sequence" VALUES ('like', 3);
INSERT INTO "sqlite_sequence" VALUES ('circle', 6);

-- ----------------------------
-- Table structure for star
-- ----------------------------
DROP TABLE IF EXISTS "star";
CREATE TABLE "star" (
  "id" integer PRIMARY KEY AUTOINCREMENT,
  "isstared" integer,
  "targetid" integer,
  "targettype" text,
  "userid" integer
);

-- ----------------------------
-- Records of "star"
-- ----------------------------
INSERT INTO "star" VALUES (1, 1, 3, 'star dynamic', 2);
INSERT INTO "star" VALUES (2, 1, 1, 'star dynamic', 7);

-- ----------------------------
-- Table structure for storey
-- ----------------------------
DROP TABLE IF EXISTS "storey";
CREATE TABLE "storey" (
  "id" integer PRIMARY KEY AUTOINCREMENT,
  "circle" integer,
  "commentnumber" integer,
  "content" text,
  "createtime" integer,
  "supportnumber" integer,
  "userid" integer
);

-- ----------------------------
-- Table structure for table_schema
-- ----------------------------
DROP TABLE IF EXISTS "table_schema";
CREATE TABLE "table_schema" (
  "id" integer PRIMARY KEY AUTOINCREMENT,
  "name" text,
  "type" integer
);

-- ----------------------------
-- Records of "table_schema"
-- ----------------------------
INSERT INTO "table_schema" VALUES (1, 'user', 0);
INSERT INTO "table_schema" VALUES (2, 'comment', 0);
INSERT INTO "table_schema" VALUES (3, 'circle', 0);
INSERT INTO "table_schema" VALUES (4, 'dynamic', 0);
INSERT INTO "table_schema" VALUES (5, 'like', 0);
INSERT INTO "table_schema" VALUES (6, 'star', 0);
INSERT INTO "table_schema" VALUES (7, 'storey', 0);
INSERT INTO "table_schema" VALUES (8, 'hide', 0);

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS "user";
CREATE TABLE "user" (
  "id" integer PRIMARY KEY AUTOINCREMENT,
  "email" text,
  "headimage" text,
  "password" text,
  "phone" text,
  "username" text
);

-- ----------------------------
-- Records of "user"
-- ----------------------------
INSERT INTO "user" VALUES (1, '1975116165@qq.com', 'http://www.westudy.ltd/upload/1/jpg/64221133-12f3-4739-a278-fa52926a1ec8.jpg', 'e10adc3949ba59abbe56e057f20f883e', 15950803241, 'yuruiqiao');
INSERT INTO "user" VALUES (2, '1990694522@qq.com', 'http://www.westudy.ltd/upload/2/jpg/af16da27-c15e-4fff-b1c4-75d1f17a6d3f.jpg', 'e10adc3949ba59abbe56e057f20f883e', 15951737601, '蓝盾');
INSERT INTO "user" VALUES (3, 'ruiqiaoyu@foxmail.com', 'http://yun918.cn/study/public/uploadfiles/img/2017-12-17-04-42-38.jpg', 'e10adc3949ba59abbe56e057f20f883e', 17805188216, '冰盾');
INSERT INTO "user" VALUES (7, 'ruiqiaoyu@gmail.com', 'http://www.westudy.ltd/upload/3/jpg/2668b263-45f9-4dbe-8aa5-13ba166b6b0f.jpg', 'e10adc3949ba59abbe56e057f20f883e', 17805188218, '张三');
INSERT INTO "user" VALUES (8, 'ruiqiaoyu@qq.com', 'http://www.westudy.ltd/upload/default.png', 'e10adc3949ba59abbe56e057f20f883e', 17805188217, '李四');

-- ----------------------------
-- Auto increment value for circle
-- ----------------------------
UPDATE "sqlite_sequence" SET seq = 6 WHERE name = 'circle';

-- ----------------------------
-- Auto increment value for dynamic
-- ----------------------------
UPDATE "sqlite_sequence" SET seq = 4 WHERE name = 'dynamic';

-- ----------------------------
-- Auto increment value for hide
-- ----------------------------
UPDATE "sqlite_sequence" SET seq = 1 WHERE name = 'hide';

-- ----------------------------
-- Auto increment value for like
-- ----------------------------
UPDATE "sqlite_sequence" SET seq = 3 WHERE name = 'like';

-- ----------------------------
-- Auto increment value for star
-- ----------------------------
UPDATE "sqlite_sequence" SET seq = 2 WHERE name = 'star';

-- ----------------------------
-- Auto increment value for table_schema
-- ----------------------------
UPDATE "sqlite_sequence" SET seq = 8 WHERE name = 'table_schema';

-- ----------------------------
-- Auto increment value for user
-- ----------------------------
UPDATE "sqlite_sequence" SET seq = 8 WHERE name = 'user';

PRAGMA foreign_keys = true;
