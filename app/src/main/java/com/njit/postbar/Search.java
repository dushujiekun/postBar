package com.njit.postbar;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.njit.postbar.model.Circle;
import com.njit.postbar.utils.Remove;

import org.litepal.LitePal;

import java.util.List;

public class Search extends Activity {
    private SearchView searchView;
    private ListView searchList;
    //定义自动完成的列表
    List<Circle> circles;
    private boolean callback = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search);
        Remove.removeTop(this);
        Intent intent = this.getIntent();
        callback = intent.getBooleanExtra("callback",false);
        circles = LitePal.findAll(Circle.class);
        String[] strings = new String[circles.size()];
        for (int i = 0; i < circles.size(); i++) {
            strings[i] = circles.get(i).getTitle();
        }
        //取消并返回
        TextView cancel = findViewById(R.id.cancel);

        //搜索框
        searchView = findViewById(R.id.search_view);
        //搜索列表
        searchList = findViewById(R.id.search_list);

        final ArrayAdapter adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, strings);
        searchList.setAdapter(adapter);
        //为ListView启动过滤
        searchList.setTextFilterEnabled(true);
        //设置SearchView自动缩小为图标
        searchView.setIconifiedByDefault(false);
        //设置该SearchView显示搜索按钮
        searchView.setSubmitButtonEnabled(true);
        //设置默认提示文字
        searchView.setQueryHint("输入您想查找的内容");
        //配置监听器
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            //点击搜索按钮时触发
            @Override
            public boolean onQueryTextSubmit(String query) {
                //此处添加查询开始后的具体时间和方法
                Toast.makeText(Search.this,"你选择了" + query,Toast.LENGTH_LONG).show();
                return false;
            }
            @Override
            public boolean onQueryTextChange(String newText) {
                //如果newText长度不为0
                if (TextUtils.isEmpty(newText)){
                    searchList.clearTextFilter();
                }else{
                    adapter.getFilter().filter(newText);
                }
                return true;
            }
        });

        searchList.setOnItemClickListener(new ListView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String string = (String) adapter.getItem(position);
                searchView.setQuery(string,true);
                if(callback){
                    String  str = (String)searchList.getItemAtPosition(position);
                    Intent intent = new Intent();
                    intent.putExtra("circleName",str);
                    setResult(RESULT_OK,intent);
                    finish();
                }
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


    }
}
