package com.njit.postbar.model;

import org.litepal.crud.LitePalSupport;

import java.io.Serializable;

/**
 * 大拇指的地方用到like
 * @date 2020/01/28
 */
public class Like extends LitePalSupport implements Serializable {

    public static final String COMMENT = "like this comment";
    public static final String DYNAMIC = "star dynamic";

    private int id;

    private Integer userId;

    private Integer targetId;

    private String targetType;

    private Boolean isLiked;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Boolean getLiked() {
        return isLiked;
    }

    public void setLiked(Boolean liked) {
        isLiked = liked;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getTargetId() {
        return targetId;
    }

    public void setTargetId(Integer targetId) {
        this.targetId = targetId;
    }

    public String getTargetType() {
        return targetType;
    }

    public void setTargetType(String targetType) {
        this.targetType = targetType;
    }

    public Boolean getIsLiked() {
        return isLiked;
    }

    public void setIsLiked(Boolean isLiked) {
        this.isLiked = isLiked;
    }
}