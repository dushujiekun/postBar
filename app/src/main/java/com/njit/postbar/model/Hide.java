package com.njit.postbar.model;

import org.litepal.crud.LitePalSupport;

import java.io.Serializable;
/**
 *@author yuruiqiao
 *@date 2020/6/6
 *@description 用于屏蔽一些用户,圈子,实体
 */
public class Hide extends LitePalSupport implements Serializable {

    public static final int USER = 1;
    public static final int CIRCLE = 2;
    public static final int DYNAMIC = 3;

    private int id;
    private int userId;
    private int targetId;
    private int targetType;
    private Boolean state;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getTargetId() {
        return targetId;
    }

    public void setTargetId(int targetId) {
        this.targetId = targetId;
    }

    public Boolean getState() {
        return state;
    }

    public void setState(Boolean state) {
        this.state = state;
    }

    public int getTargetType() {
        return targetType;
    }

    public void setTargetType(int targetType) {
        this.targetType = targetType;
    }
}
