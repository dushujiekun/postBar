package com.njit.postbar.model;

import org.litepal.crud.LitePalSupport;

import java.io.Serializable;

/**
 * 五角星的地方用到star,比如关注用户,收藏圈子,收藏某条动态
 * @date 2020/01/28
 */
public class Star extends LitePalSupport implements Serializable {

    public static final String USER = "follow user";
    public static final String CIRCLE = "star circle";
    public static final String DYNAMIC = "star dynamic";

    private int id;

    private Integer userId;

    private Integer targetId;
    
    private String targetType;

    private Boolean isStared;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getTargetId() {
        return targetId;
    }

    public void setTargetId(Integer targetId) {
        this.targetId = targetId;
    }

    public String getTargetType() {
        return targetType;
    }

    public void setTargetType(String targetType) {
        this.targetType = targetType;
    }

    public Boolean getIsStared() {
        return isStared;
    }

    public void setIsStared(Boolean isStared) {
        this.isStared = isStared;
    }
}