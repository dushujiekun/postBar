package com.njit.postbar.model;


import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Path;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;

public class RoundImageView extends androidx.appcompat.widget.AppCompatImageView{

    float width,height;

    private final static int num = 20;

    public RoundImageView(Context context) {
        this(context, null);
    }

    public RoundImageView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public RoundImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        if (Build.VERSION.SDK_INT < 18) {
            setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        }
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        width = getWidth();
        height = getHeight();
    }

    @Override
    protected void onDraw(Canvas canvas) {

        if (width > 12 && height > 12) {
            Path path = new Path();
            path.moveTo(num, 0);
            path.lineTo(width - num, 0);
            path.quadTo(width, 0, width, num);
            path.lineTo(width, height - num);
            path.quadTo(width, height, width - num, height);
            path.lineTo(num, height);
            path.quadTo(0, height, 0, height - num);
            path.lineTo(0, num);
            path.quadTo(0, 0, num, 0);
            canvas.clipPath(path);
        }

        super.onDraw(canvas);
    }
}
