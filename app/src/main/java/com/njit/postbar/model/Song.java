package com.njit.postbar.model;

import org.litepal.crud.LitePalSupport;

import java.io.Serializable;

/**
 * @author yuruiqiao
 * @date 2020/05/21
 */
public class Song extends LitePalSupport implements Serializable {

    private String songName;

    private String singerName;

    private String coverImage;

    private String songUrl;


    public String getSongName() {
        return songName;
    }

    public void setSongName(String songName) {
        this.songName = songName;
    }

    public String getSingerName() {
        return singerName;
    }

    public void setSingerName(String singerName) {
        this.singerName = singerName;
    }

    public String getCoverImage() {
        return coverImage;
    }

    public void setCoverImage(String coverImage) {
        this.coverImage = coverImage;
    }

    public String getSongUrl() {
        return songUrl;
    }

    public void setSongUrl(String songUrl) {
        this.songUrl = songUrl;
    }
}