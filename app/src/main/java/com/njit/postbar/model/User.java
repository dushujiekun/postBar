package com.njit.postbar.model;

import org.litepal.crud.LitePalSupport;

import java.io.Serializable;

/**
 * 
 * @date 2020/05/24
 */
public class User extends LitePalSupport implements Serializable {

    private int id;

    private String userName;

    private String email;

    private String phone;

    private String password;

    private String headImage;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getHeadImage() {
        return headImage;
    }

    public void setHeadImage(String headImage) {
        this.headImage = headImage;
    }
}