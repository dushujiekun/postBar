package com.njit.postbar.model;

import android.content.Context;
import android.util.Log;
import android.webkit.JavascriptInterface;

public class JavaScriptObject {
    Context context;

    //sdk17版本以上加上注解
    public JavaScriptObject(Context mContext) {
        this.context = mContext;
    }

    @JavascriptInterface
    public void getContent(String content){
        Log.e("JavaScriptObject", content);
    }
}
