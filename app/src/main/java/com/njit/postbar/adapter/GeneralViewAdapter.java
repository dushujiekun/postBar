package com.njit.postbar.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.njit.postbar.CircleDesc;
import com.njit.postbar.DynamicDesc;
import com.njit.postbar.R;
import com.njit.postbar.UserDesc;
import com.njit.postbar.model.Circle;
import com.njit.postbar.model.Dynamic;
import com.njit.postbar.model.User;

import org.litepal.LitePal;

import java.util.List;
/**
 *@author yuruiqiao
 *@date 2020/6/7
 *@description 普适适配器,只有一个图片和一个标题
 */
public class GeneralViewAdapter extends BaseAdapter {

    private Context context;

    private List<?> list;

    private int layoutSourceId,imageSourceId,nameSourceId;

    private User user;
    public GeneralViewAdapter(Context context, List<?> list,int [] SourceIds,User user) {
        this.list = list;
        this.context = context;
        this.user = user;
        layoutSourceId = SourceIds[0];
        imageSourceId = SourceIds[1];
        nameSourceId = SourceIds[2];
    }

    /**
     * 组件集合，对应list.xml中的控件
     * @author Administrator
     */
    static final class Component{
        ImageView preImage;
        TextView name;
    }
    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint({"InflateParams"})
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Component component;
        if(convertView == null){
            component = new Component();
            //获得组件,实例化组件
            convertView = LayoutInflater.from(context).inflate(layoutSourceId, null);
            component.preImage = convertView.findViewById(imageSourceId);
            component.name = convertView.findViewById(nameSourceId);
            convertView.setTag(component);
        }else{
            component = (Component)convertView.getTag();
        }
        //绑定数据
        final Object obj = list.get(position);
        if(obj instanceof Circle){
            String imageUrl = ((Circle)obj).getPreviewImageUrl();
            if("".equals(imageUrl)){
                Glide.with(context).load("http://yun918.cn/study/public/uploadfiles/img/Screenshot_20200608_010509.jpg").into(component.preImage);
            }else{
                Glide.with(context).load(imageUrl).into(component.preImage);
            }
            component.name.setText(((Circle)obj).getTitle());
            //跳往圈子详情页
            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //跳转详情页
                    Intent intent = new Intent(context, CircleDesc.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("user",user);
                    bundle.putSerializable("circle",(Circle)obj);
                    intent.putExtras(bundle);
                    context.startActivity(intent);
                }
            });
        }else if(obj instanceof User){
            String imageUrl = ((User)obj).getHeadImage();
            Glide.with(context).load(imageUrl).into(component.preImage);
            component.name.setText(((User)obj).getUserName());
            //跳往用户详情页
            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //跳转详情页
                    Intent intent = new Intent(context, UserDesc.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("user",user);
                    bundle.putSerializable("targetUser",(User)obj);
                    intent.putExtras(bundle);
                    context.startActivity(intent);
                }
            });
        }else if(obj instanceof Dynamic){
            String imageUrl = ((Dynamic)obj).getPreviewImageUrl();
            //默认动态预览图
            if("".equals(imageUrl)){
                Glide.with(context).load("http://www.westudy.ltd/upload/1/jpg/db77bfe9-b268-4475-817a-2b465976c751.jpg").into(component.preImage);
            }else{
                Glide.with(context).load(imageUrl).into(component.preImage);
            }
            component.name.setText(((Dynamic)obj).getTitle());
            //跳往动态详情页
            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //跳转详情页
                    User poster = LitePal.find(User.class,((Dynamic) obj).getUserId());
                    Intent intent = new Intent(context, DynamicDesc.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("poster",poster);
                    bundle.putSerializable("user",user);
                    bundle.putSerializable("dynamic",(Dynamic)obj);
                    intent.putExtras(bundle);
                    context.startActivity(intent);
                }
            });
        }

        return convertView;
    }
}