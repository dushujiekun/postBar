package com.njit.postbar.utils;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Check {
    /**
     * 判断电话格式是否正确，以及是否重复
     * 2020-01-18
     */
    public static boolean checkPhone(String phoneNum) {
        if (phoneNum.length() == 11) {
            //利用格式转换异常来判断是否全部是数字
            try {
                long realNum = Long.parseLong(phoneNum);
            } catch (NumberFormatException ne) {
                return false;
            }
            return true;
        }
        return false;
    }

    /**
     * 判断邮箱格式是否正确，以及是否重复
     * 2020-01-18
     */
    public static boolean checkEmail(String email) {
        if (email.length() > 7) {
            //判断邮箱格式，查询网页得知
            Pattern emailPattern = Pattern.compile("\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*");
            Matcher matcher = emailPattern.matcher(email);
            return matcher.find();

        }
        return false;
    }
}
