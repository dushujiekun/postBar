package com.njit.postbar.utils;

import android.annotation.SuppressLint;

import java.text.SimpleDateFormat;
import java.util.Date;
/**
 *@author yuruiqiao
 *@date 2020/6/6
 *@description 日期转换类
 */
public class DateFormat {
    @SuppressLint("SimpleDateFormat")
    private static SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
    //把时间戳转换为几分钟或几小时前或几天前
    public static String timeDetails(Date date) {
        long interval = ((new Date()).getTime() - date.getTime())/1000;

        if (interval <= 30){
            return "刚刚";
        }else if (interval < 60){
            return interval+"秒前";
        }else if (interval < 3600){
            return interval/60+"分钟前";
        }else if (interval < 86400){
            return interval/3600+"小时前";
        }else if (interval < 2592000){
            return interval/86400+"天前";
        }else{
            return formatter.format(date);
        }
    }
}
