package com.njit.postbar.fragments;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.njit.postbar.Post;
import com.njit.postbar.R;
import com.njit.postbar.adapter.DynamicListAdapter;
import com.njit.postbar.model.Dynamic;
import com.njit.postbar.model.Hide;
import com.njit.postbar.model.Like;
import com.njit.postbar.model.Star;
import com.njit.postbar.model.User;

import org.litepal.LitePal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class DynamicFragment extends Fragment {
    private User user;
    private Context mContext;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private ListView listView;
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        this.mContext = getActivity();
        Bundle bundle = getArguments();
        assert bundle != null;
        user = (User) bundle.getSerializable("user");
    }
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        final View root = inflater.inflate(R.layout.fragment_dynamic, container, false);
        ImageView add = root.findViewById(R.id.add_post);
        listView = root.findViewById(R.id.activity_list);
        mSwipeRefreshLayout = root.findViewById(R.id.refresh);
        List<Map<String, Object>> list = getData();
        listView.setAdapter(new DynamicListAdapter(getActivity(), list));
        listView.setDivider(null);
        //监听器
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(user!=null){
                    Intent intent = new Intent();
                    intent.setClass(root.getContext(), Post.class);
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("user", user);
                    intent.putExtras(bundle);
                    startActivity(intent);
                }else{
                    Toast.makeText(mContext, "请先登录", Toast.LENGTH_LONG).show();
                }
            }
        });
        //下拉刷新
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                List<Map<String, Object>> list = getData();
                listView.setAdapter(new DynamicListAdapter(getActivity(), list));
                Toast.makeText(mContext, "刷新成功", Toast.LENGTH_LONG).show();
                mSwipeRefreshLayout.setRefreshing(false);//刷新完成
            }
        });
        return root;
    }
    private List<Map<String, Object>> getData(){
        List<Map<String, Object>> list= new ArrayList<>();
        List<Integer> removedIds = new ArrayList<>();
        List<Integer> starIds = new ArrayList<>();
        List<Integer> likeIds = new ArrayList<>();
        if(user!=null){
            //找到用户点赞的所有动态
            List<Like> likes = LitePal.where("userid=? and targettype=? and isliked=?",
                    user.getId()+"",Like.DYNAMIC+"","1").find(Like.class);
            //找到用户收藏的所有动态
            List<Star> stars = LitePal.where("userid=? and targettype=? and isstared=?",
                    user.getId()+"",Star.DYNAMIC+"","1").find(Star.class);
            //找到用户屏蔽的所有动态
            List<Hide> hideDynamics = LitePal.where("userid=? and targettype=? ",
                    user.getId()+"",Hide.DYNAMIC+"").find(Hide.class);
            //这三个用于截取id
            for (int i = 0; i < hideDynamics.size(); i++) {
                removedIds.add(hideDynamics.get(i).getTargetId());
            }
            for (int i = 0; i < stars.size(); i++) {
                starIds.add(stars.get(i).getTargetId());
            }
            for (int i = 0; i < likes.size(); i++) {
                likeIds.add(likes.get(i).getTargetId());
            }
        }
        List<Dynamic> dynamics = LitePal.findAll(Dynamic.class);
        for (int i = 0; i < dynamics.size(); i++) {
            //过滤掉屏蔽的动态
            if(!removedIds.contains(dynamics.get(i).getId())){
                Map<String, Object> map= new HashMap<>();
                map.put("dynamic", dynamics.get(i));
                map.put("currentUser",user);
                //找到点赞数
                int likeNum = LitePal.where("targetid=? and targettype=? and isliked=?",
                        dynamics.get(i).getId()+"",Like.DYNAMIC,"1").count(Like.class);
                Dynamic dynamic = LitePal.find(Dynamic.class,dynamics.get(i).getId());
                dynamic.setLikeNumber(likeNum);
                dynamic.save();
                if(starIds.contains(dynamics.get(i).getId())){
                    map.put("isStared",true);
                }else{
                    map.put("isStared",false);
                }
                if(likeIds.contains(dynamics.get(i).getId())){
                    map.put("isLiked",true);
                }else{
                    map.put("isLiked",false);
                }
                list.add(map);
            }
        }
        return list;
    }
}
