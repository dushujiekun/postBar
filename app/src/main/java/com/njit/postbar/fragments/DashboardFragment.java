package com.njit.postbar.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.njit.postbar.R;
import com.njit.postbar.adapter.GeneralViewAdapter;
import com.njit.postbar.model.Circle;
import com.njit.postbar.model.User;

import org.litepal.LitePal;
import java.util.List;

public class DashboardFragment extends Fragment {
    private User user;
    private View root;
    private GridView gridView,gridView2,gridView3,gridView4;
    private Context mContext;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private List<Circle> dailyList;
    private List<Circle> interests;
    private List<User> users;
    private int [] sourceId;
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        this.mContext = getActivity();
        Bundle bundle = getArguments();
        assert bundle != null;
        user = (User) bundle.getSerializable("user");
    }
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        root = inflater.inflate(R.layout.fragment_dashboard, container, false);
        sourceId = new int[]{R.layout.item_basic_verticle,R.id.image,R.id.name};
        mSwipeRefreshLayout = root.findViewById(R.id.refresh);
        gridView = root.findViewById(R.id.grid);
        gridView2 = root.findViewById(R.id.grid2);
        gridView3 = root.findViewById(R.id.grid3);
        gridView4 = root.findViewById(R.id.grid4);
        dailyList = LitePal.findAll(Circle.class);
        interests = LitePal.findAll(Circle.class);
        users = LitePal.findAll(User.class);
        gridView.setAdapter(new GeneralViewAdapter(getActivity(), dailyList,sourceId,user));
        gridView2.setAdapter(new GeneralViewAdapter(getActivity(), interests,sourceId,user));
        gridView3.setAdapter(new GeneralViewAdapter(getActivity(), users,sourceId,user));
        gridView4.setAdapter(new GeneralViewAdapter(getActivity(), users,sourceId,user));
        setGridView(dailyList,gridView);
        setGridView(interests,gridView2);
        setGridView(users,gridView3);
        setGridView(users,gridView4);
        //下拉刷新
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                dailyList = LitePal.findAll(Circle.class);
                interests = LitePal.findAll(Circle.class);
                users = LitePal.findAll(User.class);
                gridView.setAdapter(new GeneralViewAdapter(getActivity(), dailyList,sourceId,user));
                gridView2.setAdapter(new GeneralViewAdapter(getActivity(), interests,sourceId,user));
                gridView3.setAdapter(new GeneralViewAdapter(getActivity(), users,sourceId,user));
                gridView4.setAdapter(new GeneralViewAdapter(getActivity(), users,sourceId,user));
                Toast.makeText(mContext, "刷新成功", Toast.LENGTH_LONG).show();
                mSwipeRefreshLayout.setRefreshing(false);//刷新完成
            }
        });

        return root;
    }

    /**
     *@author yuruiqiao
     *@date 2020/6/6
     *@description 设置GirdView参数，绑定数据
     */
    private void setGridView(List<?> list,GridView gridView) {
        int size = list.size();
        int length = 100;
        DisplayMetrics dm = new DisplayMetrics();
        ((Activity)mContext).getWindowManager().getDefaultDisplay().getMetrics(dm);
        float density = dm.density;
        int gridViewWidth = (int) (size * (length + 4) * density);
        int itemWidth = (int) (length * density);

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                gridViewWidth, LinearLayout.LayoutParams.MATCH_PARENT);
        gridView.setLayoutParams(params); // 设置GirdView布局参数,横向布局的关键
        gridView.setColumnWidth(itemWidth); // 设置列表项宽
        gridView.setHorizontalSpacing(5); // 设置列表项水平间距
        gridView.setStretchMode(GridView.NO_STRETCH);
        gridView.setNumColumns(size); // 设置列数量=列表集合数

        GeneralViewAdapter adapter = new GeneralViewAdapter(mContext.getApplicationContext(),list,sourceId,user);
        gridView.setAdapter(adapter);
    }
}
