package com.njit.postbar.fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.njit.postbar.R;
import com.njit.postbar.adapter.GeneralViewAdapter;
import com.njit.postbar.adapter.ViewPagerAdapter;
import com.njit.postbar.model.Circle;
import com.njit.postbar.model.Dynamic;
import com.njit.postbar.model.Star;
import com.njit.postbar.model.User;

import org.litepal.LitePal;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.viewpager.widget.ViewPager;

public class FavoriteFragment extends Fragment {

    private User user;
    private Context mContext;
    private List<View> views = new ArrayList<>();
    private ViewPager viewPager;
    private SwipeRefreshLayout swipeRefreshLayout;

    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        this.mContext = getActivity();
        Bundle bundle = getArguments();
        assert bundle != null;
        user = (User) bundle.getSerializable("user");
    }

    @SuppressLint("InflateParams")
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_favorites, container, false);

        views.add(LayoutInflater.from(mContext).inflate(R.layout.favorite_list_circle, null));
        views.add(LayoutInflater.from(mContext).inflate(R.layout.favorite_list_dynamic, null));
        views.add(LayoutInflater.from(mContext).inflate(R.layout.favorite_list_following, null));
        //获取到三个listView
        ListView circleListView = views.get(0).findViewById(R.id.favorite_circle_list);
        ListView dynamicListView = views.get(1).findViewById(R.id.favorite_dynamic_list);
        ListView followingListView = views.get(2).findViewById(R.id.favorite_following_list);
        int [] sourceId = new int[]{R.layout.item_basic_horizon,R.id.image,R.id.name};
        //查询出来用户收藏的圈子 动态 用户
        List<Circle> circles = new ArrayList<>();
        List<Dynamic> dynamics = new ArrayList<>();
        List<User> followings = new ArrayList<>();

        if(user!=null){
            String sql = "targettype=? and userid=? and isstared = 1";
            //查询出来用户收藏的所有 圈子 动态 用户
            List<Star> stars_circle = LitePal.where(sql,Star.CIRCLE,user.getId()+"").find(Star.class);
            List<Star> stars_dynamic = LitePal.where(sql,Star.DYNAMIC,user.getId()+"").find(Star.class);
            List<Star> stars_following = LitePal.where(sql,Star.USER,user.getId()+"").find(Star.class);

            for (int i = 0; i < stars_circle.size(); i++) {
                Circle circle = LitePal.find(Circle.class,stars_circle.get(i).getTargetId());
                circles.add(circle);
            }
            for (int i = 0; i < stars_dynamic.size(); i++) {
                Dynamic dynamic = LitePal.find(Dynamic.class,stars_dynamic.get(i).getTargetId());
                dynamics.add(dynamic);
            }
            for (int i = 0; i < stars_following.size(); i++) {
                User user = LitePal.find(User.class,stars_following.get(i).getTargetId());
                followings.add(user);
            }
        }

        //适配器
        circleListView.setAdapter(new GeneralViewAdapter(getActivity(), circles,sourceId,user));
        dynamicListView.setAdapter(new GeneralViewAdapter(getActivity(), dynamics,sourceId,user));
        followingListView.setAdapter(new GeneralViewAdapter(getActivity(), followings,sourceId,user));
        //去掉框线
        circleListView.setDivider(null);
        dynamicListView.setDivider(null);
        followingListView.setDivider(null);
        viewPager = ((ViewPager) root.findViewById(R.id.view_pager));
        viewPager.setAdapter(new ViewPagerAdapter(mContext,views));
        swipeRefreshLayout = root.findViewById(R.id.refresh);
        //下拉刷新监听器
        swipeRefreshLayout.setOnRefreshListener(mOnRefreshListener);
        return root;
    }


    private SwipeRefreshLayout.OnRefreshListener mOnRefreshListener = new SwipeRefreshLayout.OnRefreshListener() {
        @Override
        public void onRefresh() {
            viewPager.setAdapter(new ViewPagerAdapter(mContext,views));
            Toast.makeText(mContext, "刷新成功", Toast.LENGTH_LONG).show();
            swipeRefreshLayout.setRefreshing(false);//刷新完成
        }
    };
}
