package com.njit.postbar.fragments;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.RotateAnimation;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.njit.postbar.CreateCircle;
import com.njit.postbar.R;
import com.njit.postbar.adapter.CircleListAdapter;
import com.njit.postbar.model.Circle;
import com.njit.postbar.model.User;

import org.litepal.LitePal;
import java.util.List;

import static android.view.animation.Animation.REVERSE;

public class CircleFragment extends Fragment {
    //传过来的User对象
    private User user;
    //用于存放activity,便于使用
    private Context mContext;
    //圈子列表
    private ListView listView;
    //左下角的创建按钮
    private ImageView createCircle,createCircle2;
    //下拉刷新
    private SwipeRefreshLayout mSwipeRefreshLayout;
    //用于控制动画线程
    private boolean flag = true;
    //初始化
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        this.mContext = getActivity();
        //接收传过来的对象
        Bundle bundle = getArguments();
        assert bundle != null;
        user = (User) bundle.getSerializable("user");
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        //圈子页面
        View root = inflater.inflate(R.layout.fragment_circle, container, false);
        //找到控件
        listView = root.findViewById(R.id.recommend_parent);
        mSwipeRefreshLayout = root.findViewById(R.id.refresh);
        createCircle = root.findViewById(R.id.create_circle);
        createCircle2 = root.findViewById(R.id.create_circle_yellow);
        //动画
        startAnimationThread();
        //查询出来的所有圈子
        List<Circle> circles = LitePal.findAll(Circle.class);
        listView.setAdapter(new CircleListAdapter(getActivity(), circles,user));
        //去掉框线
        listView.setDivider(null);

        //下拉刷新
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                List<Circle> circles = LitePal.findAll(Circle.class);
                listView.setAdapter(new CircleListAdapter(getActivity(), circles,user));
                Toast.makeText(mContext, "刷新成功", Toast.LENGTH_LONG).show();
                mSwipeRefreshLayout.setRefreshing(false);//刷新完成
            }
        });

        //创建圈子
        createCircle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(user==null){
                    showDialog();
                }else{
                    confirmDialog(new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            //跳转登录页面,传递对象
                            Intent intent = new Intent();
                            intent.setClass(mContext, CreateCircle.class);
                            Bundle bundle = new Bundle();
                            bundle.putSerializable("user", user);
                            intent.putExtras(bundle);
                            startActivity(intent);
                        }
                    });
                }
            }
        });


        return root;
    }
    /**
     *@author yuruiqiao
     *@date 2020/5/26
     *@description 动画函数
     */
    private AnimationSet animation(int time){
        //旋转动画
        RotateAnimation ra = new RotateAnimation(0, -180, RotateAnimation.RELATIVE_TO_SELF, 0.5f, RotateAnimation.RELATIVE_TO_SELF, 0.5f);
        ScaleAnimation sa = (ScaleAnimation) AnimationUtils.loadAnimation(mContext, R.anim.scale);
        //透明度变化
        AlphaAnimation aa = new AlphaAnimation(0.8f, 0.5f);
        ra.setRepeatCount(2);
        sa.setRepeatCount(2);
        //动画集合
        AnimationSet aSet = new AnimationSet(true);
        aSet.setRepeatMode(REVERSE);
        aSet.setDuration(time/2);
        aSet.addAnimation(ra);
        aSet.addAnimation(sa);
        aSet.addAnimation(aa);
        aSet.setFillAfter(true);
        return aSet;
    }
    /**
     *@author yuruiqiao
     *@date 2020/5/26
     *@description 动画循环
     */
    private void circleAnim(ImageView view){
        while(flag){
            int time = (int)(Math.random()*5000)+3000;
            view.startAnimation(animation(time));
            try {
                Thread.sleep(time);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
    /**
     *@author yuruiqiao
     *@date 2020/6/7
     *@description 动画线程
     */
    public void startAnimationThread(){

        //前景色的动画
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                circleAnim(createCircle);
            }
        });
        thread.start();
        //背景色动画
        Thread thread2 = new Thread(new Runnable() {
            @Override
            public void run() {
                circleAnim(createCircle2);
            }
        });
        thread2.start();
    }
    /**
     *@author yuruiqiao
     *@date 2020/6/6
     *@description 选择确认框
     */
    private void confirmDialog(DialogInterface.OnClickListener listener){
        AlertDialog.Builder builder  = new AlertDialog.Builder(mContext);
        builder.setTitle("请确认") ;
        builder.setMessage("是否创建圈子？") ;
        builder.setPositiveButton("是😁",listener);
        builder.setNegativeButton("否",null);
        builder.show();
    }
    /**
     *@author yuruiqiao
     *@date 2020/6/6
     *@description 提示框
     */
    private void showDialog(){
        AlertDialog.Builder builder  = new AlertDialog.Builder(mContext);
        builder.setTitle("提示") ;
        builder.setMessage("请先登录😅") ;
        builder.setPositiveButton("我知道了" ,  null );
        builder.show();
    }

    @Override
    public void onDestroy() {
        flag = false;
        super.onDestroy();
    }
}
