package com.njit.postbar;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.njit.postbar.adapter.DynamicListAdapter;
import com.njit.postbar.model.Circle;
import com.njit.postbar.model.Dynamic;
import com.njit.postbar.model.Hide;
import com.njit.postbar.model.Like;
import com.njit.postbar.model.Star;
import com.njit.postbar.model.User;
import com.njit.postbar.utils.Remove;

import org.litepal.LitePal;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

public class CircleDesc extends Activity {

    private User user;
    private Circle circle;
    private ListView listView;
    private Context mContext;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private TextView circleName,staredNumber;
    private ImageView background;
    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.desc_circle);
        Remove.removeTop(this);
        mContext = getBaseContext();
        //用户信息
        Intent intent = this.getIntent();
        user = (User) intent.getSerializableExtra("user");
        circle = (Circle) intent.getSerializableExtra("circle");
        mSwipeRefreshLayout = findViewById(R.id.refresh);
        circleName = findViewById(R.id.circle_title);
        staredNumber = findViewById(R.id.star_number);
        background = findViewById(R.id.background);
        circleName.setText(circle.getTitle());
        int stars = LitePal.where("targettype=? and targetid=?",Star.CIRCLE,circle.getId()+"").count(Star.class);
        staredNumber.setText(""+stars);
        String url = circle.getPreviewImageUrl();
        if(!"".equals(url)){
            Glide.with(this).load(url).into(background);
        }else{
            Glide.with(this).load("http://yun918.cn/study/public/uploadfiles/img/Screenshot_20200608_010509.jpg").into(background);
        }
        listView = findViewById(R.id.dynamic_list_in_circle);
        listView.setAdapter(new DynamicListAdapter(this,getData()));
        listView.setDivider(null);
        //下拉刷新
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                listView.setAdapter(new DynamicListAdapter(mContext, getData()));
                Toast.makeText(mContext, "刷新成功", Toast.LENGTH_LONG).show();
                mSwipeRefreshLayout.setRefreshing(false);//刷新完成
            }
        });
    }

    private List<Map<String, Object>> getData(){
        List<Map<String, Object>> list= new ArrayList<>();
        List<Integer> removedIds = new ArrayList<>();
        List<Integer> starIds = new ArrayList<>();
        List<Integer> likeIds = new ArrayList<>();
        if(user!=null){
            //找到用户点赞的所有动态
            List<Like> likes = LitePal.where("userid=? and targettype=? and isliked=?",
                    user.getId()+"",Like.DYNAMIC+"","1").find(Like.class);
            //找到用户收藏的所有动态
            List<Star> stars = LitePal.where("userid=? and targettype=? and isstared=?",
                    user.getId()+"",Star.DYNAMIC+"","1").find(Star.class);
            //找到用户屏蔽的所有动态
            List<Hide> hideDynamics = LitePal.where("userid=? and targettype=? ",
                    user.getId()+"",Hide.DYNAMIC+"").find(Hide.class);
            //这三个用于截取id
            for (int i = 0; i < hideDynamics.size(); i++) {
                removedIds.add(hideDynamics.get(i).getTargetId());
            }
            for (int i = 0; i < stars.size(); i++) {
                starIds.add(stars.get(i).getTargetId());
            }
            for (int i = 0; i < likes.size(); i++) {
                likeIds.add(likes.get(i).getTargetId());
            }
        }
        List<Dynamic> dynamics;
        if(circle!=null){
            dynamics = LitePal.where("circle = ?",circle.getTitle()).find(Dynamic.class);
        }else{
            dynamics = LitePal.where("circle = ?","学习").find(Dynamic.class);
        }
        for (int i = 0; i < dynamics.size(); i++) {
            //过滤掉屏蔽的动态
            if(!removedIds.contains(dynamics.get(i).getId())){
                Map<String, Object> map= new HashMap<>();
                map.put("dynamic", dynamics.get(i));
                map.put("currentUser",user);
                //找到点赞数
                int likeNum = LitePal.where("targetid=? and targettype=? and isliked=?",
                        dynamics.get(i).getId()+"",Like.DYNAMIC,"1").count(Like.class);
                Dynamic dynamic = LitePal.find(Dynamic.class,dynamics.get(i).getId());
                dynamic.setLikeNumber(likeNum);
                dynamic.save();
                if(starIds.contains(dynamics.get(i).getId())){
                    map.put("isStared",true);
                }else{
                    map.put("isStared",false);
                }
                if(likeIds.contains(dynamics.get(i).getId())){
                    map.put("isLiked",true);
                }else{
                    map.put("isLiked",false);
                }
                list.add(map);
            }
        }
        return list;
    }
}