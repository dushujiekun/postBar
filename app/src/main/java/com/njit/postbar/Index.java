package com.njit.postbar;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.njit.postbar.model.User;
import com.njit.postbar.fragments.DynamicFragment;
import com.njit.postbar.fragments.DashboardFragment;
import com.njit.postbar.fragments.FavoriteFragment;
import com.njit.postbar.fragments.CircleFragment;
import com.njit.postbar.utils.Remove;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

/**
 *@author yuruiqiao
 *@date 2020/6/4
 *@description
 */

public class Index extends AppCompatActivity {

    private Fragment mCurrentFragment = null;
    private Fragment[] fragments;
    private ImageView headImage;
    private Button search;
    private User user;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.index);
        Remove.removeTop(this);

        baseInfo();
        //跳转到搜索页面
        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //跳转页面,传递对象
                Intent intent = new Intent();
                intent.setClass(Index.this,Search.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable("user", user);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
        //点击头像的按钮
        headImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(user != null){
                    //跳转信息页面,传递对象
                    Intent intent = new Intent();
                    intent.setClass(Index.this, MyInfo.class);
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("user", user);
                    intent.putExtras(bundle);
                    startActivity(intent);
                }else {
                    //跳转登录页面
                    confirmDialog(new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Intent intent = new Intent();
                            intent.setClass(Index.this,Login.class);
                            startActivity(intent);
                            finish();
                        }
                    });
                }
            }
        });
    }

    /**
     *@author yuruiqiao
     *@date 2020/5/29
     *@description 页面切换
     */
    private void showFragment(int position) {

        Fragment fragment = fragments[position];

        if (mCurrentFragment != fragment) {

            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.hide(mCurrentFragment);

            mCurrentFragment = fragment;

            if (!fragment.isAdded())
                transaction.add(R.id.container, fragment);
            else
                transaction.show(fragment);

            transaction.commitAllowingStateLoss();
        }
    }

    private void baseInfo(){
        CircleFragment circleFragment = new CircleFragment();
        DashboardFragment dashboardFragment = new DashboardFragment();
        DynamicFragment dynamicFragment = new DynamicFragment();
        FavoriteFragment favoriteFragment = new FavoriteFragment();
        fragments = new Fragment[]{circleFragment, dashboardFragment, dynamicFragment, favoriteFragment};
        BottomNavigationView navigation = findViewById(R.id.nav_view);
        navigation.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.navigation_home: showFragment(0);return true;
                    case R.id.navigation_dashboard: showFragment(1);return true;
                    case R.id.navigation_activity: showFragment(2);return true;
                    case R.id.navigation_favorites: showFragment(3);return true;
                }
                return false;
            }
        });
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        //装入第一页
        transaction.add(R.id.container, circleFragment).commit();
        mCurrentFragment = circleFragment;
        //头像
        headImage = findViewById(R.id.head_image);
        //搜索框
        search = findViewById(R.id.search_index);
        //用户信息
        Intent intent = this.getIntent();
        user = (User) intent.getSerializableExtra("user");
//        user = LitePal.find(User.class,1);

        Bundle bundle = new Bundle();
        bundle.putSerializable("user", user);
        //给下面四个fragment传递User对象
        circleFragment.setArguments(bundle);
        dashboardFragment.setArguments(bundle);
        dynamicFragment.setArguments(bundle);
        favoriteFragment.setArguments(bundle);
        if(user != null){
            Glide.with(this).load(user.getHeadImage()).into(headImage);

        }else{
            //默认头像
            headImage.setBackground(getDrawable(R.drawable.ic_login_default));
        }
    }
    /**
     *@author yuruiqiao
     *@date 2020/6/6
     *@description 选择确认框
     */
    private void confirmDialog(DialogInterface.OnClickListener listener){
        AlertDialog.Builder builder  = new AlertDialog.Builder(Index.this);
        builder.setTitle("请登录") ;
        builder.setMessage("是否前往登录？") ;
        builder.setPositiveButton("是", listener);
        builder.setNegativeButton("否",null);
        builder.show();
    }
}