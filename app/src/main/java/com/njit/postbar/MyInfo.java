package com.njit.postbar;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.njit.postbar.model.RoundImageView;
import com.njit.postbar.model.UpLoadBean;
import com.njit.postbar.model.User;
import com.njit.postbar.server.MyServer;
import com.njit.postbar.utils.Remove;

import com.bumptech.glide.Glide;
import com.njit.postbar.utils.UploadImage;

import org.litepal.LitePal;

import java.io.File;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
/**
 *@author yuruiqiao
 *@date 2020/6/6
 *@description 用户基本信息,待完善
 */
public class MyInfo extends UploadImage {
    private RelativeLayout updateNickname,updatePhone,updatePassword;
    private Button logout,login;
    private RoundImageView headImage;
    private User user;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_info);
        Remove.removeTop(this);
        init();
        listeners();
    }

    /**
     *@author yuruiqiao
     *@date 2020/6/5
     *@description 初始化
     */
    private void init(){
        headImage = findViewById(R.id.userInfo_headImage);
        updateNickname = findViewById(R.id.update_nickname);
        updatePhone = findViewById(R.id.update_phone);
        updatePassword = findViewById(R.id.update_password);
        logout = findViewById(R.id.logout);
        login = findViewById(R.id.login);
        TextView nickname = findViewById(R.id.nickname);
        TextView phone = findViewById(R.id.phone);
        Intent intent = this.getIntent();
        user = (User) intent.getSerializableExtra("user");
        //用户头像
        if(user==null){
            Glide.with(this).load("http://www.westudy.ltd/upload/default.png").into(headImage);
            login.setVisibility(View.VISIBLE);
        }else{
            Glide.with(this).load(user.getHeadImage()).into(headImage);
            nickname.setText(user.getUserName());
            phone.setText(user.getPhone());
            logout.setVisibility(View.VISIBLE);
        }
    }

    /**
     *@author yuruiqiao
     *@date 2020/6/5
     *@description 监听器
     */
    @SuppressLint("ClickableViewAccessibility")
    private void listeners(){
        //修改头像
        headImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(user!=null){
                    //执行父类的上传图片方法
                    MyInfo.this.takesD();
                }else{
                    Toast.makeText(getBaseContext(),"请先登录", Toast.LENGTH_LONG).show();
                }
            }
        });
        updateNickname.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(user!=null){
                    Toast.makeText(getBaseContext(),"昵称修改成功", Toast.LENGTH_LONG).show();
                }else{
                    Toast.makeText(getBaseContext(),"请先登录", Toast.LENGTH_LONG).show();
                }
            }
        });

        updatePhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(user!=null){
                    Toast.makeText(getBaseContext(),"手机号修改成功", Toast.LENGTH_LONG).show();
                }else{
                    Toast.makeText(getBaseContext(),"请先登录", Toast.LENGTH_LONG).show();
                }
            }
        });

        updatePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(user!=null){
                    Toast.makeText(getBaseContext(),"密码修改成功", Toast.LENGTH_LONG).show();
                }else{
                    Toast.makeText(getBaseContext(),"请先登录", Toast.LENGTH_LONG).show();
                }
            }
        });

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getBaseContext(),"跳转中", Toast.LENGTH_LONG).show();
            }
        });

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getBaseContext(),"跳转中", Toast.LENGTH_LONG).show();
            }
        });
    }
    /**
     *@author yuruiqiao
     *@date 2020/6/5
     *@description 必须重写方法不然会出现线程错误
     */
    @Override
    protected void updateFile(File file) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(MyServer.Url)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
        MyServer myServer = retrofit.create(MyServer.class);
        //文件封装
        RequestBody requestBody = RequestBody.create(MediaType.parse("image/jpg"),file);
        MultipartBody.Part filePart = MultipartBody.Part.createFormData(
                "file", file.getName(), requestBody);
        //文本封装
        RequestBody requestBody1 = RequestBody.create(MediaType.parse("multipart/form-data"),"img");
        final Observable<UpLoadBean> upload = myServer.upload(requestBody1,filePart);
        upload.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<UpLoadBean>() {
                    @Override
                    public void onSubscribe(Disposable d) {}
                    @Override
                    public void onNext(UpLoadBean upLoadBean) {
                        if (upLoadBean != null && upLoadBean.getCode() == 200) {
                            //这边获得图片的地址并修改头像url
                            User newUser = LitePal.find(User.class,user.getId());
                            newUser.setHeadImage(upLoadBean.getData().getUrl());
                            newUser.save();
                            //这一步更新user
                            user = newUser;
                            //更新图像
                            Glide.with(MyInfo.this).load(user.getHeadImage()).into(headImage);
                            //下面是提示信息
                            Log.d("lzj", upLoadBean.getData().getUrl());
                            Toast.makeText(getBaseContext(), upLoadBean.getData().getUrl(), Toast.LENGTH_LONG).show();
                        } else {
                            Log.d("lzj", "failed");
                        }
                    }
                    @Override
                    public void onError(Throwable e) {}
                    @Override
                    public void onComplete() {}
                });
    }
}
