package com.njit.postbar;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.njit.postbar.model.Dynamic;
import com.njit.postbar.model.User;
import com.njit.postbar.utils.DateFormat;
import com.njit.postbar.utils.HTML;
import com.njit.postbar.utils.Remove;

public class DynamicDesc extends Activity {
    private ImageView headImage;
    private User user;
    @SuppressLint({"SetJavaScriptEnabled", "SetTextI18n"})
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.desc_dynamic);
        Remove.removeTop(this);
        Intent intent = this.getIntent();
        User poster = (User)intent.getSerializableExtra("poster");
        user = (User)intent.getSerializableExtra("user");
        Dynamic dynamic = (Dynamic)intent.getSerializableExtra("dynamic");
        headImage = findViewById(R.id.head_image_desc);
        TextView posterName = findViewById(R.id.poster_name_desc);
        TextView time = findViewById(R.id.poster_time_desc);
        TextView title = findViewById(R.id.title_desc);
        WebView webView = findViewById(R.id.desc_content);
        TextView likeNum = findViewById(R.id.like_num_desc);
        TextView commentNum = findViewById(R.id.comment_desc_num);
        Glide.with(this).load(poster.getHeadImage()).into(headImage);

        posterName.setText(poster.getUserName());
        time.setText(DateFormat.timeDetails(dynamic.getCreateTime()));
        title.setText(dynamic.getTitle());
        likeNum.setText(dynamic.getLikeNumber()+"");
        commentNum.setText(dynamic.getCommentNumber()+"");

        //内容
        String content = dynamic.getContent();
        String newUrl = HTML.setDESC(content);
        WebSettings webSettings = webView.getSettings();
        //如果访问的页面中要与Javascript交互
        webSettings.setJavaScriptEnabled(true);
        webView.loadDataWithBaseURL(null,newUrl,"text/html",
                "utf-8",null);

        setListeners();
    }
    /**
     *@author yuruiqiao
     *@date 2020/6/6
     *@description 监听器
     */
    public void setListeners(){
        //点击头像访问用户首页
        headImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }
}
